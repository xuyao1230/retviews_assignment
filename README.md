Analysing products data

This repository contain the following files:

original_data: the directory of all original files

requirements.txt: all python libraries for running the script

codes: the directory of python code, including a python script, a jupyter notebook and a test script, the former two files contain the same code, ipynb file has extra visualisation part

Other three pdf files: 3 visualisation pictures of data


First step install all dependencies:

    - Open a shell in the file were you found this README
    - sudo pip3 install -r requirements.txt // NB = if this command don't work try : sudo pip install -r requirements.txt

Next step, run the following command lines:

    - If you are running in a GCP VM, and you have all files in your bucket, type:
        python3 Retviews_Xuyao.py <Your bucket name>

    - Otherwise just read data from local and process:
        python3 Retviews_Xuyao.py

    - Alternatively, you can open the Retviews_XuyaoZhang.ipynb in your Jupyter notebook to check the code and pictures