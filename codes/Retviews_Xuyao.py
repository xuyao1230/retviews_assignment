import os
import sys
from google.cloud import storage
import json
import pandas as pd
import numpy as np
from scipy.stats import zscore
import itertools
import multiprocessing

class Products(object):
    """
    param item: a json object

    variable product_id: unique id of product, string
    variable title_and_des: title and description of product, dict, e.g. {title: description}
    variable colors: all colors of a product, list of string, e.g. ['black', 'red']
    variable images: all images of a product, list of lists, e.g. [[all images of color1], [all images of color2]]
    variable price_hierarchy: all price hierarchy of a product, list of tuples, e.g. [(price_color1, previous_color1),(price_color2, previous_color2)] 
    variable deltas: all deltas of a product, list of float, e.g. [delta of color1, delta of color2]
    variable all_path: category pathes of a product, dict
    """
    def __init__(self, item):
        
        self.product_id=item['product_id']
        self.title_and_des={item['main_title']['en']: item['description']['en']}
        self.colors=item['details']['colors']
        self.images=[] 
        self.price_hierarchy=[]
        self.deltas=[]
        self.all_path={}
        
        for color in self.colors:
            self.images.append(np.array(item['images'][color]))
            price=item['price_hierarchy'][color]['price']['GBP']
            previous_price=item['price_hierarchy'][color]['previous_price']['GBP']
            
            # check price format, price should be float, if previous price is null, fill it with current price
            try:
                price=float(price)
                if previous_price=='':
                    previous_price=price
                else:
                    previous_price=float(previous_price)
                    
                self.price_hierarchy.append((price,previous_price))
                #self.price_hierarchy.append(np.array([price,previous_price]))
                self.deltas.append(previous_price-price)
                
            except ValueError:
                print("The price format of %s is wrong" %(self.product_id))
                

                

        for path in item['full_path']:
            if path[0] not in self.all_path:
                self.all_path[path[0]]=[path[1]]
            else:
                self.all_path[path[0]].append(path[1])
         
            
        
def read_file_to_list(file_dir, queue):
    """
    This function reads lines from a file, and puts all transformed products to a queue

    parameter file_dir: file path
    parameter queue: queue for communication betweeen different processes
    """
    original_data=[]
    f = open(file_dir, 'r')
    for item in f:
        json_item=json.loads(item)
        product=Products(json_item)
        original_data.append(np.array([product.product_id, product.title_and_des, product.colors, product.images, product.price_hierarchy, product.deltas, product.all_path]))
        
    f.close()
    queue.put(np.array(original_data))


def read_from_local():
    """
    This function opens a local directory, and creates several processes to process data files asynchronously
    return a list of all products
    """
    all_data=[]
    file_lists = os.listdir('../original_data')
    processes = []
    manager = multiprocessing.Manager()
    queue = manager.Queue()
    
    for file in file_lists:
        processes.append(multiprocessing.Process(target=read_file_to_list,args=("../original_data/"+file,queue)))

    for process in processes:
        process.start()

    for process in processes:  
        process.join()
        
    while True:
        if not queue.empty():
            all_data.append(queue.get())
        else:
            break
            
    return all_data

def cloud_json_tolist(blob):
    """
    This function reads data from a cloud json file, and puts all transformed products to a list

    parameter blob: a blob file in a cloud bucket

    return an array of all json products of this file
    """
    original_data=[]
    
    bt = blob.download_as_string()

    df=pd.read_json(bt, lines=True)
    
    json_list=df.to_dict(orient='records')
    
    for item in json_list:
        
        product=Products(item)
        original_data.append(np.array([product.product_id, product.title_and_des, product.colors, product.images, product.price_hierarchy, product.deltas, product.all_path]))
    
    return np.array(original_data)

def read_from_cloud(bucket_name):
    """
    This function access the google cloud storage and process data files one by one
    return a list of all products
    """
    all_data=[]
    
    #os.environ["GOOGLE_APPLICATION_CREDENTIALS"]="9ac575ff975a.json"

    client = storage.Client()

    bucket = client.get_bucket(bucket_name) #"xuyao_retviews_bucket"
    
    blobs = client.list_blobs(bucket_or_name=bucket)
    
    for blob in blobs:
        blob_file=bucket.get_blob(blob.name)
        all_data.append(cloud_json_tolist(blob_file))
        
    return all_data


def split_rows(dataframe, split_by, column_list):
    """
    This function splits a dataframe when one or more columns contain multiple values, for example

    original dataframe:

    Name      Score
    May       [89,90,88]
    Lee       [90,43]

    splitted dataframe:
 
    Name      Score
    May        89
    May        90
    May        88
    Lee        90
    Lee        43

    parameter dataframe: the dataframe to be splitted
    parameter split_by: the cloumn based on which to split
    parameter column_list: the list of columns which need to be splitted

    return splitted dataframe
    """
    row_len=list(map(len, dataframe[split_by].values))
    rows=[]
    
    for i in dataframe.columns:
        if i in column_list:
            #row=np.concatenate(dataframe[i].values)
            if i == 'price_hierarchy':
                row=np.array(list(itertools.chain.from_iterable(dataframe[i].values)), dtype=np.dtype('float, float'))
            else:
                row=np.array(list(itertools.chain.from_iterable(dataframe[i].values)))
        else:
            row=np.repeat(dataframe[i].values, row_len)
            
        rows.append(row)
    
    return pd.DataFrame(np.dstack(tuple(rows))[0], columns=dataframe.columns)



def find_categories(dataframe):
    """
    This function find all categories in the data
    Specifically, there are two-level categories of products represented as a dictionary, e.g. {'first_category1': [second category1, second category2...]}

    parameter dataframe: the dataframe of products
    return the dictionary which contains first categories as keys and corresponding sencond categories as values  
    """
    path_column_list=dataframe['all_path'].tolist()
    category_dic={}

    for path in path_column_list:
        for key in path:
            if key not in category_dic:
                category_dic[key]=path[key]
            else:
                category_dic[key]+=path[key]
            
    for key in category_dic:
        # remove duplicates in the list
        category_dic[key] = list(dict.fromkeys(category_dic[key]))
        
    return category_dic


def price_1st_check(dataframe, highest_discount):
    """
    This function checks whether current price is lower than or equal to previous price
    If there is something wrong, print ids and price hierarchies for further analysis

    parameter dataframe: the dataframe of products
    parameter highest_discount: the highest discount

    return error ids
    """
    ddf=dataframe['price_hierarchy'].apply(lambda x: x[0]>x[1] or x[0]<x[1]*highest_discount)
    #ddf2=dataframe['price_hierarchy'].apply(lambda x: x[0]<x[1] and x[0]<x[1]*highest_discount)
    index_list=ddf.index[ddf==True].tolist()
    #index_list2=ddf2.index[ddf2==True].tolist()
    outlier=[]
    if len(index_list)==0:
        print("No errors found")
    else:
        print("There may be something wrong with these products")
        sub_dataframe1=dataframe.iloc[index_list]
        #sub_dataframe2=dataframe.iloc[index_list2]
        print(sub_dataframe1['price_hierarchy'])
        outlier=[sub_dataframe1.loc[i,'id'] for i in index_list]
    
    return outlier


def price_outlier_check(dataframe, threshold):
    """
    This function checks errors on prices (e.g. a price should be 170.0, but the data is 17.0)
    based on z-score, namely how far a price is away from the mean value

    parameter dataframe: the dataframe of all products
    parameter threshold: the threshold of z-score distribution

    return 
    outlier: the list of error ids
    digit_diff: the list of how many digits the decimal point should move
    """
    price_list=dataframe['price_hierarchy'].tolist()
    #current_price=np.array([x[0] for x in price_list])
    previous_price=np.array([x[1] for x in price_list])
    digits_average=len(str(int(np.mean(previous_price))))
    zscore_price=zscore(previous_price)
    outlier = [] 
    digit_diff=[]
    
    for i, score in enumerate(zscore_price):
        if abs(score)>threshold:
            digit=len(str(int(previous_price[i])))
            diff=digits_average - digit
            if diff != 0:
                outlier.append(dataframe.iloc[i]['id']) 
                digit_diff.append(diff)
        
    return outlier, digit_diff


def check_error_by_category(category_dict, dataframe, threshold):
    """
    This function checks price errores based on each category

    parameter category_dict: category dictionary
    parameter dataframe: the dataframe of products
    parameter threshold: the threshold of z-score distribution

    return

    all_errors: the list of all error ids
    all_digits: the list of how many digits the decimal point should move
    """
    all_errors=[]
    all_digits=[]
    for key in category_dict:
        new_column=dataframe['all_path'].apply(lambda x: key in x)
        sub_dataframe=dataframe.iloc[new_column.index[new_column==True].tolist()]
        error_ids, digits=price_outlier_check(sub_dataframe, threshold)
        print(f"Category {key} has errors:")
        print(dataframe.loc[dataframe['id'].isin(error_ids)]['price_hierarchy'])
        all_errors+=error_ids
        all_digits+=digits
        
    return all_errors, all_digits


def correct_1st_error(dataframe, error_ids, highest_discount):
    """
    This function corrects wrong prices detected by first check

    parameter dataframe: the dataframe of products
    parameter error_ids: the list of error ids
    parameter highest_discount: a float number indicates the highest discount

    return the dataframe which is already corrected
    """
    for i, error in enumerate(error_ids):

        former_current=0
        former_previous=0
        
        (former_current, former_previous)=tuple(dataframe.loc[dataframe['id'] == error]['price_hierarchy'])[0]
        
        while former_current>former_previous:
            former_current=former_current/10.0
            
        while former_current<former_previous*highest_discount:
            former_current=former_current*10.0
            
        row_index=dataframe.index[dataframe['id'] == error].tolist()[0]

        dataframe.loc[row_index,'price_hierarchy']=(former_current, former_previous)
        dataframe.loc[row_index,'deltas']=former_previous-former_current
            
    return dataframe


def correct_decimal_point(dataframe, error_ids, diff):
    """
    This function corrects wrong prices by moving the decimal points

    parameter dataframe: the dataframe of products
    parameter error_ids: the list of error ids
    parameter diff: the list of how many digits the decimal point should move

    return the dataframe which is already corrected
    """
    for i, error in enumerate(error_ids):
  
        former_current=0
        former_previous=0
        
        
        (former_current, former_previous)=tuple(dataframe.loc[dataframe['id'] == error]['price_hierarchy'])[0]
        new_current=former_current*pow(10, diff[i])
        new_previous=former_previous*pow(10, diff[i])
        #price_h=tuple(dataframe.loc[dataframe['id'] == error]['price_hierarchy'])
        row_index=dataframe.index[dataframe['id'] == error].tolist()[0]
        dataframe.loc[row_index,'price_hierarchy']=(new_current, new_previous)
        dataframe.loc[row_index,'deltas']=new_previous-new_current
            
    return dataframe


if __name__ == '__main__':

    column_names = ["id", "title_and_des", "colors","images","price_hierarchy","deltas","all_path"]
    highest_discount=0.5
    all_data=[]
    print("Start processing data")
    
    #no extra arguments
    if len(sys.argv)==1:
        all_data=read_from_local()
    elif len(sys.argv)==2:
        all_data=read_from_cloud(list(sys.argv)[1])
    
    # remove extra [], then in this list, each element is a product
    unduplicated_data=list(itertools.chain.from_iterable(all_data))
    
    # convert list to dataframe
    df = pd.DataFrame(data=unduplicated_data, columns=column_names)  

    all_len=len(df)
    print(f"There are {all_len} products in the original json files")

    #remove duplicates in the dataframe based on products id
    df = df.drop_duplicates(subset=['id'])#.reset_index()
    product_no=len(df)

    print(f"There are {product_no} unique products finally")
    
    # split the dataframe by colors
    df_all=split_rows(df, 'colors',['colors', 'images', 'deltas', 'price_hierarchy'])
    
    
    print("A sample of the data:")
    print(df_all.sample(5))
    
    print("Price 1st check: current price equal to or lower than previous price:")
    first_error=price_1st_check(df_all, highest_discount)
    
    print("After correcting, the products are:")
    correct_df1=correct_1st_error(df_all, first_error, highest_discount)
    print(correct_df1.loc[correct_df1['id'].isin(first_error)]['price_hierarchy'])
    
    print("Price 2nd check: decimal point errors")
    
    print("The categories of all products are shown below:")
    categories=find_categories(df_all)
    for key in categories:
        print(key)
        print(categories[key])
        
    print("Check errors by the main category:")
    all_errors, all_digits=check_error_by_category(categories, correct_df1, 2)
    print("After correcting, the products are:")
    correct_df2=correct_decimal_point(correct_df1, all_errors, all_digits)
    print(correct_df2.loc[correct_df2['id'].isin(all_errors)]['price_hierarchy'])
    

    #Store everything in a proper way
    
    correct_df2[['deltas']] = correct_df2[['deltas']].astype(float)
    correct_df2[['price','previous_price']]=correct_df2['price_hierarchy'].apply(pd.Series)
    final_df=correct_df2[["id", "title_and_des", "colors","images","price","previous_price", "deltas"]]
    
    print("=====================================Data Scitistics=====================================")
    print(final_df.describe(include='all'))
    print("=========================================================================================")
    
    
    
    






