import codes.Retviews_Xuyao
import pytest
import pandas as pd
import numpy as np


@pytest.fixture
def single_json():
    return {"meta": {"upload": "2020-07-01 23:36:02.248510"}, "url": {"en": ["https://uk.claudiepierlot.com/en/features/capsule-ceremonie/120boco/CFPTO00270.html?dwvar_CFPTO00270_color=A004"]}, "product_id": "https://uk.claudiepierlot.com/en/features/capsule-ceremonie/120boco/CFPTO00270.html?dwvar_CFPTO00270_color=A004", "source": "Claudiepierlot", "country": ["uk"], "lang": "en", "ref": "CFPTO00270", "sex": "women", "main_title": {"en": "Flowing V neck top"}, "images": {"ECRU - A004": ["https://uk.claudiepierlot.com/dw/image/v2/BCND_PRD/on/demandware.static/-/Sites-claudie-catalog-master-H13/default/dw25c65769/images/preAH15/Claudie_CFPTO00270-A004_H_1.jpg?sw=595&sh=872&cx=0&cy=0&cw=1255&ch=1840", "https://uk.claudiepierlot.com/dw/image/v2/BCND_PRD/on/demandware.static/-/Sites-claudie-catalog-master-H13/default/dw5c10a8d6/images/preAH15/Claudie_CFPTO00270-A004_H_2.jpg?sw=595&sh=872&cx=0&cy=0&cw=1255&ch=1840", "https://uk.claudiepierlot.com/dw/image/v2/BCND_PRD/on/demandware.static/-/Sites-claudie-catalog-master-H13/default/dw94a21712/images/preAH15/Claudie_CFPTO00270-A004_H_3.jpg?sw=595&sh=872&cx=0&cy=0&cw=1255&ch=1840", "https://uk.claudiepierlot.com/dw/image/v2/BCND_PRD/on/demandware.static/-/Sites-claudie-catalog-master-H13/default/dwcfd97f15/images/preAH15/Claudie_CFPTO00270-A004_H_4.jpg?sw=595&sh=872&cx=0&cy=0&cw=1255&ch=1840", "https://uk.claudiepierlot.com/dw/image/v2/BCND_PRD/on/demandware.static/-/Sites-claudie-catalog-master-H13/default/dw47aa0554/images/preAH15/Claudie_CFPTO00270-A004_H_5.jpg?sw=595&sh=872&cx=0&cy=0&cw=1255&ch=1840"]}, "price_hierarchy": {"type": "color", "ECRU - A004": {"price": {"GBP": "175.0"}, "previous_price": {"GBP": ""}}}, "details": {"colors": ["ECRU - A004"], "material": "Main fabric: 100% polyester  Braid: 100% polyester  Secondary fabric: 82% triacetate, 18% polyester", "sizes": {"ECRU - A004": ["6", "8", "10", "12"]}, "sizes_not_available": {"ECRU - A004": []}, "all_sizes": {"ECRU - A004": ["12", "10", "8", "6"]}}, "description": {"en": "This flowing top offers a timeless elegant look. The romantic guipure lace detailing on the cuffs prettily enhances the understated contemporary cut. Wear this top loosely tucked into cropped tailored trousers for an office outfit or for a smart urban style. Just add loafers. For a more casual effect, chose high-waisted wide jeans and low-top boots."}, "color_snippets": {"ECRU - A004": "https://uk.claudiepierlot.com/on/demandware.static/-/Sites-claudie-catalog-front/default/dw5ebdbb3c/imagesSwatches/A004.jpg"}, "outlet_color": {"ECRU - A004": "false"}, "excluweb_color": {"ECRU - A004": "false"}, "full_path": [["clothes", "all collection"], ["clothes", "italian romance"]], "category_urls": ["https://uk.claudiepierlot.com/en/categories/see-all-collection/", "https://uk.claudiepierlot.com/en/features/capsule-ceremonie/"], "matching_codes": [], "tags": []}

@pytest.fixture
def correct_df():
    return pd.DataFrame(np.array([['1','shirt','blue',['link1','link2'],(199,200),1.0],['1','shirt','black',['link1','link2','link3'],(30,80),50],['2','tshirt','red',['link1','link2','link3'],(190,320),130]]), columns=['id','name','colors','images','price_hierarchy','deltas'])


@pytest.fixture
def wrong_df1():
    return pd.DataFrame(np.array([[1, (1000, 200), 100], [2, (100, 150), 50], [3, (20, 40), 20], [4, (3000, 40), -2960]]), columns=['id','price_hierarchy','deltas'])

@pytest.fixture
def wrong_df2():
    return pd.DataFrame(np.array([[1, (1000, 1000), 0], [2, (100, 150), 50], [3, (1, 1), 0], [4, (120, 120), 0]]), columns=['id','price_hierarchy', 'deltas'])

def test_products_class(single_json):
    product=codes.Retviews_Xuyao.Products(single_json)
    assert product.product_id=="https://uk.claudiepierlot.com/en/features/capsule-ceremonie/120boco/CFPTO00270.html?dwvar_CFPTO00270_color=A004"
    assert product.title_and_des=={"Flowing V neck top": "This flowing top offers a timeless elegant look. The romantic guipure lace detailing on the cuffs prettily enhances the understated contemporary cut. Wear this top loosely tucked into cropped tailored trousers for an office outfit or for a smart urban style. Just add loafers. For a more casual effect, chose high-waisted wide jeans and low-top boots."}
    assert product.colors==["ECRU - A004"]
    assert (a==b for a,b in zip(product.images[0], ["https://uk.claudiepierlot.com/dw/image/v2/BCND_PRD/on/demandware.static/-/Sites-claudie-catalog-master-H13/default/dw25c65769/images/preAH15/Claudie_CFPTO00270-A004_H_1.jpg?sw=595&sh=872&cx=0&cy=0&cw=1255&ch=1840", "https://uk.claudiepierlot.com/dw/image/v2/BCND_PRD/on/demandware.static/-/Sites-claudie-catalog-master-H13/default/dw5c10a8d6/images/preAH15/Claudie_CFPTO00270-A004_H_2.jpg?sw=595&sh=872&cx=0&cy=0&cw=1255&ch=1840", "https://uk.claudiepierlot.com/dw/image/v2/BCND_PRD/on/demandware.static/-/Sites-claudie-catalog-master-H13/default/dw94a21712/images/preAH15/Claudie_CFPTO00270-A004_H_3.jpg?sw=595&sh=872&cx=0&cy=0&cw=1255&ch=1840", "https://uk.claudiepierlot.com/dw/image/v2/BCND_PRD/on/demandware.static/-/Sites-claudie-catalog-master-H13/default/dwcfd97f15/images/preAH15/Claudie_CFPTO00270-A004_H_4.jpg?sw=595&sh=872&cx=0&cy=0&cw=1255&ch=1840", "https://uk.claudiepierlot.com/dw/image/v2/BCND_PRD/on/demandware.static/-/Sites-claudie-catalog-master-H13/default/dw47aa0554/images/preAH15/Claudie_CFPTO00270-A004_H_5.jpg?sw=595&sh=872&cx=0&cy=0&cw=1255&ch=1840"][0]))
    assert product.price_hierarchy==[(175.0, 175.0)]
    assert product.deltas == [0.0]
    assert product.all_path=={"clothes":["all collection", "italian romance"]}
    
    
def test_split_rows(correct_df):
    df = pd.DataFrame(np.array([['1','shirt',['blue','black'],[['link1','link2'],['link1','link2','link3']],[(199,200),(30,80)],[1.0,50]],['2','tshirt',['red'],[['link1','link2','link3']],[(190,320)],[130]]]), columns=['id','name','colors','images','price_hierarchy','deltas'])
    column_list=['colors','images','price_hierarchy','deltas']
    pd.testing.assert_frame_equal(correct_df, codes.Retviews_Xuyao.split_rows(df,'colors',column_list))      
    

def test_find_categories():
    df=pd.DataFrame(np.array([[1,{"clothes": ["all collection", "good style"]}],[2, {"clothes":["all collection", "beautiful"],"sale":["all collection"]}],[3, {"shoes":["man"]}]]), columns=['id', 'all_path'])
    result_dict={"clothes":["all collection", "good style", "beautiful"],"sale":["all collection"],"shoes":["man"] }
    assert result_dict==codes.Retviews_Xuyao.find_categories(df)

def test_price_1st_check(wrong_df1):
    id_list=[1, 4]
    assert (a==b for a, b in zip(id_list, codes.Retviews_Xuyao.price_1st_check(wrong_df1, 0.5)))

def test_price_outlier_check(wrong_df2):
    id_list=[1, 3]
    assert (a==b for a, b in zip(id_list, codes.Retviews_Xuyao.price_outlier_check(wrong_df2, 2)))

def test_correct_1st_error(wrong_df1):
    df=pd.DataFrame(np.array([[1, (100, 200), 100], [2, (100, 150), 50], [3, (20, 40), 20], [4, (30, 40), 10]]), columns=['id','price_hierarchy','deltas'])
    pd.testing.assert_frame_equal(df, codes.Retviews_Xuyao.correct_1st_error(wrong_df1, [1,4], 0.5))
    

def test_correct_decimal_point(wrong_df2):
    df=pd.DataFrame(np.array([[1, (100, 100), 0], [2, (100, 150), 50], [3, (100, 100), 0], [4, (120, 120), 0]]), columns=['id','price_hierarchy','deltas'])
    pd.testing.assert_frame_equal(df, codes.Retviews_Xuyao.correct_decimal_point(wrong_df2, [1,3], [-1, 2]))



